const express = require('express');
const { check } = require('express-validator');
const router = express.Router();
const isAuth = require('../middleware/user-auth');
const { getSearch, postSearch } = require('../controllers/user/search');
const { getBooking, postBooking } = require('../controllers/user/homepage');
const { postBookTicket } = require('../controllers/user/booking');
const { getProfile } = require('../controllers/user/profile');
const {
  getReservation,
  searchReservation,
} = require('../controllers/user/reservation');

router.get('/', getSearch);
router.post(
  '/',
  [
    check('from').not().isEmpty().withMessage('From field is empty'),
    check('to').not().isEmpty().withMessage('To field is empty'),
    check('date').not().isEmpty().withMessage('Date field is empty'),
  ],
  postSearch
);

// // @Desc Get booking page for certain
// router.get('/booking', isAuth, getBooking);

// @ Desc Get booking page
router.post('/booking', isAuth, postBooking);

// Booking the tickets
router.post(
  '/bookTicket',
  isAuth,
  [check('bookingDate').not().isEmpty().withMessage('Date field is empty')],
  postBookTicket
);

// Get the profile
router.get('/profile', isAuth, getProfile);
// Get reservation
router.get('/reservation', isAuth, getReservation);
// Search user reservation
router.post('/reservation', isAuth, searchReservation);

module.exports = router;
