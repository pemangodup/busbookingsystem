const express = require('express');
const router = express.Router();
const isAuth = require('../middleware/admin-auth');
const {
  getDashboard,
  postLogin,
  getBusList,
  getAddBus,
  postAddBus,
  getEditBus,
  postEditBus,
  deleteBus,
} = require('../controllers/admin/bus');
const {
  getAddSchedule,
  postAddSchedule,
  getEditSchedule,
  postEditSchedule,
  deleteSchedule,
} = require('../controllers/admin/schedule');
const {
  getReservation,
  searchReservationById,
} = require('../controllers/admin/reservation');

router.get('/dashboard', isAuth, getDashboard);
router.post('/dashboard', isAuth, postLogin);

router.get('/list-bus', isAuth, getBusList);
router.get('/add-bus', isAuth, getAddBus);
router.post('/add-bus', isAuth, postAddBus);
router.get('/edit-bus/:id', isAuth, getEditBus);
router.post('/post-edit', isAuth, postEditBus);
router.get('/delete-bus/:id', isAuth, deleteBus);

// Adding and getting schedule
router.get('/add-schedule', isAuth, getAddSchedule);
router.post('/add-schedule', isAuth, postAddSchedule);
router.get('/edit-schedule/:id', isAuth, getEditSchedule);
router.post('/postEditSchedule', isAuth, postEditSchedule);
router.get('/delete-schedule/:scheduleId', isAuth, deleteSchedule);

// Reservation
router.get('/reservation', isAuth, getReservation);

// Search reservation by id
router.post('/reservation', isAuth, searchReservationById);

module.exports = router;
