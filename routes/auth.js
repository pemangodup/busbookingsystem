const express = require('express');
const User = require('../models/user');
const { body } = require('express-validator');

const router = express.Router();
const { getLogin, postLogin } = require('../controllers/auth/login');

const { getSignup, postSignup } = require('../controllers/auth/sign-up');
const {
  getEmailVerify,
  postEmailVerify,
} = require('../controllers/auth/email-verify');
const {
  getPasswordReset,
  postPasswordReset,
} = require('../controllers/auth/password-reset');

const { logout } = require('../controllers/auth/logout');
// GET login routes
router.route('/').get(getLogin);

// POST login routes
router.post(
  '/',
  [
    body('email').isEmail().withMessage('Is not an email.'),
    body('password', 'Password is wrong')
      .isLength({ min: 5, max: 20 })
      .isAlphanumeric()
      .trim(),
  ],
  postLogin
);

// GET signup routes
router.route('/signup').get(getSignup);

// POST sign up
router.post(
  '/signup',
  [
    body('email')
      .isEmail()
      .custom((value) => {
        return User.findOne({ where: { email: value } }).then((user) => {
          if (user) {
            console.log(user);
            return Promise.reject('Email already exist.');
          }
        });
      }),
    body(
      'password',
      'Should be min 5 and max 20 character and should consist of an alpha numeric only.'
    )
      .isLength({ min: 5, max: 20 })
      .isAlphanumeric()
      .trim(),
    body('confirmPassword').custom((value, { req }) => {
      if (value !== req.body.password) {
        throw new Error('Password confirmation does not match password.');
      }
      return true;
    }),
  ],
  postSignup
);

// logout routes
router.get('/logout', logout);

// Get password change, email checkup page
router.get('/emailVerify', getEmailVerify);

// Post verify email and send token in mail
router.post('/emailVerify', postEmailVerify);

// Get password resetting page
router.get('/emailVerify/:token', getPasswordReset);

// Post reset password
router.post('/resetPassword', postPasswordReset);

// Exporting modules
module.exports = router;
