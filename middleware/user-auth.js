module.exports = (req, res, next) => {
  if (req.session.user) {
    next();
  } else {
    console.log('User is not logged in');
    res.redirect('/auth');
  }
};
