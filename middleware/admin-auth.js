module.exports = (req, res, next) => {
  if (req.session.user) {
    if (req.session.user.userType != 'admin') {
      res.redirect('/auth');
    }
    next();
  } else {
    console.log('Not logged in as an admin.');
    res.redirect('/auth');
  }
};
