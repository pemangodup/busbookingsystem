const User = require('../../models/user');
const bcrypt = require('bcryptjs');
const { validationResult } = require('express-validator');
const nodemailer = require('nodemailer');
const sendgridTransport = require('nodemailer-sendgrid-transport');
const crypto = require('crypto');
const Bus = require('../../models/bus');
const Reservation = require('../../models/reservation');
//  Get login page
exports.getLogin = (req, res, next) => {
  let errorMessage = req.flash('error');
  if (errorMessage.length > 0) {
    errorMessage = errorMessage;
  } else {
    errorMessage = null;
  }
  console.log(`${errorMessage}`.red);
  res.render('../views/auth/login.ejs', { errorMessage: errorMessage });
};

//  Logging in and storing the user in a session
exports.postLogin = async (req, res, next) => {
  const { email, password } = req.body;
  const user = await User.findOne({ where: { email: email } });
  if (user) {
    if (user.userType == 'admin') {
      bcrypt
        .compare(password, user.password)
        .then(async (doMatch) => {
          if (doMatch) {
            req.session.isLoggedIn = true;
            req.session.user = user;
            const reservation = await Reservation.findAll();
            const bus = await Bus.findAll();
            res.render('../views/admin/dashboard.ejs', {
              userCheck: 'admin',
              user: 'Dashboard',
              reservation: Object.keys(reservation).length,
              totalBus: Object.keys(bus).length,
            });
          } else {
            console.log('Password does not match'.rainbow);
            await req.flash('error', 'Password does not match');
            res.redirect('/auth');
          }
        })
        .catch((err) => {
          console.log(err);
        });
    } else {
      bcrypt
        .compare(password, user.password)
        .then((doMatch) => {
          if (doMatch) {
            req.session.isLoggedIn = true;
            req.session.user = user;
            res.redirect('/');
          } else {
            console.log('Password does not match'.red);
            req.flash('error', 'Password does not match');
            res.redirect('/auth');
          }
        })
        .catch((err) => {
          console.log(err);
        });
    }
  } else {
    console.log('No user with that email.'.red);
    req.flash('error', 'No user with that email.');
    res.redirect('/auth');
  }
};
