const User = require('../../models/user');
const bcrypt = require('bcryptjs');
const { validationResult } = require('express-validator');
const nodemailer = require('nodemailer');
const sendgridTransport = require('nodemailer-sendgrid-transport');
const crypto = require('crypto');

//  Get email verilfy page
exports.getEmailVerify = (req, res, next) => {
  console.log('Email verify page');
  res.render('../views/auth/emailVerify.ejs', {
    resetPassword: null,
    token: null,
  });
};

//  Email verilfy and send token in mail
exports.postEmailVerify = (req, res, next) => {
  const email = req.body.email;

  crypto.randomBytes(32, async (err, buffer) => {
    if (err) {
      console.log(err);
      return res.redirect('/auth/login');
    }
    const token = buffer.toString('hex');
    const user = await User.findAll({ where: { email: email } });
    if (!user[0]) {
      res.render('../views/auth/emailVerify.ejs', {
        resetPassword: null,
      });
    } else {
      // Storing generated token in database in respective user
      await User.update(
        { token: token, tokenExpiration: Date.now() + 3600000 },
        { where: { email: email } }
      );
      // Now sending the generated token as a mail
      // Creating the transporter
      const transporter = await nodemailer.createTransport(
        sendgridTransport({
          auth: {
            api_key: process.env.API_KEY,
          },
        })
      );
      // Now Sending mail with the help of transporter
      await transporter.sendMail({
        to: email,
        from: 'pngodup123@gmail.com',
        subject: 'Password Reset',
        html: `<p>Your requested a password reset</p> 
           
          <p>Clink this link to reset password <a href="http://localhost:3000/auth/emailVerify/${token}">link </a></p>`,
      });
      res.render('../views/auth/emailSend.ejs');
    }
  });
};
