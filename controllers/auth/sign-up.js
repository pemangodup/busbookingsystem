const User = require('../../models/user');
const bcrypt = require('bcryptjs');
const { validationResult } = require('express-validator');
const nodemailer = require('nodemailer');
const sendgridTransport = require('nodemailer-sendgrid-transport');
const crypto = require('crypto');

//  Rendering the signup page
exports.getSignup = async (req, res, next) => {
  const message = await req.flash('error');
  await res.render('../views/auth/signup.ejs', { errorMessage: message });
};

//  Storing user in the database
exports.postSignup = async (req, res, next) => {
  const name = req.body.name;
  const email = req.body.email;
  const password = req.body.password;
  const userType = 'passenger';
  const errorsResult = validationResult(req);

  if (!errorsResult.isEmpty()) {
    await req.flash('error', `Some fields are empty`);
    return res.redirect('/auth/signup');
  }
  try {
    const hashedPassword = await bcrypt.hash(password, 12);
    console.log(hashedPassword);
    console.log('user created.');
    await User.create({
      name: name,
      email: email,
      password: hashedPassword,
      userType: userType,
    });
    res.redirect('/auth');
  } catch (err) {
    console('Server error.');
    console.log(err);
  }
};
