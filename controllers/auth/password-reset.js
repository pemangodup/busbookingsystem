const User = require('../../models/user');
const bcrypt = require('bcryptjs');
const { validationResult } = require('express-validator');
const nodemailer = require('nodemailer');
const sendgridTransport = require('nodemailer-sendgrid-transport');
const crypto = require('crypto');

// Password reset page
exports.getPasswordReset = (req, res, next) => {
  res.render('../views/auth/emailVerify.ejs', {
    resetPassword: 'reset',
    token: req.params.token,
  });
};

// Password reset
exports.postPasswordReset = async (req, res, next) => {
  const { token, newPassword, confirmPassword } = req.body;
  const user = await User.findAll({ where: { token: token } });
  console.log(`${token}`.rainbow);
  console.log(`This is id ${user[0].id}`.rainbow);
  if (user) {
    const id = user[0].id;
    const hashedPassword = await bcrypt.hash(newPassword, 12);
    console.log(`${hashedPassword}`.rainbow);

    await User.update(
      {
        password: hashedPassword,
        token: null,
        tokenExpiration: null,
      },
      { where: { id: id } }
    )
      .then((result) => {
        console.log(`Password reset complete`.rainbow);
      })
      .catch((err) => {
        console.log(err);
      });
    res.redirect('/auth/');
  } else {
    console.log(`No user found`.rainbow);
  }
};
