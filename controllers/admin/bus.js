const Bus = require('../../models/bus');
const Reservation = require('../../models/reservation');
const Schedule = require('../../models/schedule');
const { Op } = require('sequelize');
exports.getDashboard = async (req, res, next) => {
  const reservation = await Reservation.findAll();
  const bus = await Bus.findAll();
  res.render('../views/admin/dashboard.ejs', {
    user: 'Admin Dashboard',
    userCheck: 'admin',
    reservation: Object.keys(reservation).length,
    totalBus: Object.keys(bus).length,
  });
};

//
exports.postLogin = (req, res, next) => {
  res.render('../views/admin/dashboard.ejs', { user: 'admin' });
};

// Getting the list of of bus schedule
exports.getBusList = async (req, res, next) => {
  Schedule.findAll()
    .then((schedule) => {
      res.render('../views/admin/list-bus.ejs', {
        sch: schedule,
        user: 'Schedule List',
        userCheck: 'admin',
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

// Getting the add bus page rendering bus data
exports.getAddBus = async (req, res, next) => {
  const bus = await Bus.findAll();
  res.render('../views/admin/add-bus.ejs', {
    bus,
    editing: null,
    user: 'Add Bus',
    userCheck: 'admin',
    title: 'Add Bus',
    errorMessage: null,
  });
};

// Posting bus into the database
exports.postAddBus = async (req, res, next) => {
  const busName = req.body.busName;
  const busNumber = req.body.busNumber;
  const busPlateNumber = req.body.busPlateNumber;
  let capacity =
    'a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11,b12';
  if (busName != '' && busNumber != '' && busPlateNumber != '') {
    // Checking whether the bus number already registered or not
    const bus = await Bus.findAll({
      where: { busNumber: busNumber, busPlateNumber: busPlateNumber },
    });
    if (!bus.length) {
      Bus.create({
        busName,
        busNumber,
        busPlateNumber,
        capacity,
      });
    } else {
      console.log('Bus already exist'.red);
    }
    const busData = await Bus.findAll();
    res.render('../views/admin/add-bus.ejs', {
      bus: busData,
      editing: null,
      user: 'Add Bus',
      userCheck: 'admin',
      errorMessage: 'Bus Added',
    });
  } else {
    console.log('Some field are empty'.red);
    const busData = await Bus.findAll();
    res.render('../views/admin/add-bus.ejs', {
      bus: busData,
      editing: null,
      user: 'Add Bus',
      userCheck: 'admin',
      errorMessage: 'Some fields are empty',
    });
  }
};

// Editing the bus
exports.getEditBus = async (req, res, next) => {
  const busId = req.params.id;
  const updatedBus = await Bus.findByPk(busId);
  const bus = await Bus.findAll();
  const idForUpdate = await Bus.findAll({ where: { id: busId } });
  res.render('../views/admin/add-bus.ejs', {
    updatedBus: updatedBus,
    bus: bus,
    updatePurposeBus: idForUpdate,
    editing: 'editmode',
    user: 'Edit Bus',
    userCheck: 'admin',
    errorMessage: null,
  });
};

// Saving edited bus
exports.postEditBus = async (req, res, next) => {
  const id = req.body.busId;
  const updatedBusName = req.body.busName;
  const updatedBusNumber = req.body.busNumber;
  const updatedBusPlateNumber = req.body.busPlateNumber;
  const updatedCapacity = req.body.capacity;
  const bus = await Bus.findByPk(id);
  bus.set({
    busName: updatedBusName,
    busNumber: updatedBusNumber,
    busPlateNumber: updatedBusPlateNumber,
    capacity: updatedCapacity,
  });
  await bus.save();
  res.redirect('/admin/add-bus');
};

// Deleting the bus
exports.deleteBus = async (req, res, next) => {
  const id = req.params.id;
  const bus = await Bus.findByPk(id);
  await bus.destroy();
  console.log('Deleted.');
  res.redirect('/admin/add-bus');
};
