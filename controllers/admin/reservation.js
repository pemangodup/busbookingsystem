const Bus = require('../../models/bus');
const Reservation = require('../../models/reservation');
const Schedule = require('../../models/schedule');
const { Op } = require('sequelize');

// @Desc    Get all the reservation
exports.getReservation = async (req, res, next) => {
  const reservation = await Reservation.findAll();
  res.render('../views/admin/reservation-list.ejs', {
    reservation,
    user: 'Reservation List',
    userCheck: 'admin',
  });
};

// @Desc    Search reservation by email, name and date
exports.searchReservationById = async (req, res, next) => {
  const searchOption = req.body.searchOption;
  const searchBy = req.body.search;
  console.log(`${searchBy}`.red);
  try {
    // Search by date
    if (searchOption == 'byDate') {
      const reservation = await Reservation.findAll({
        where: {
          bookingDate: searchBy,
        },
      });
      res.render('../views/admin/reservation-list.ejs', {
        reservation: reservation,
        search: 'search',
        user: 'Reservation List',
        userCheck: 'admin',
      });
    }
    // Search by email id
    if (searchOption == 'byEmail') {
      const reservation = await Reservation.findAll({
        where: {
          bookedBy: searchBy,
        },
      });
      res.render('../views/admin/reservation-list.ejs', {
        reservation: reservation,
        search: 'search',
        user: 'Reservation List',
        userCheck: 'admin',
      });
    }
    // Search by name
    if (searchOption == 'byName') {
      const reservation = await Reservation.findAll({
        where: {
          name: searchBy,
        },
      });
      res.render('../views/admin/reservation-list.ejs', {
        reservation: reservation,
        search: 'search',
        user: 'Reservation List',
        userCheck: 'admin',
      });
    }
  } catch (error) {
    console.log(`Error: ${error.message}`.red);
  }
};
