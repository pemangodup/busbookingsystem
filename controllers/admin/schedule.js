const Bus = require('../../models/bus');
const Reservation = require('../../models/reservation');
const Schedule = require('../../models/schedule');
const { Op } = require('sequelize');

// Getting the add schedule page
exports.getAddSchedule = async (req, res, next) => {
  const allData = await Bus.findAll();
  Bus.findAll()
    .then((bus) => {
      if (!bus) {
        return res.render('../views/admin/add-schedule.ejs', {
          bus: null,
          editing: null,
          fullData: allData,
          user: 'Add Schedule',
          userCheck: 'admin',
          errorMessage: null,
        });
      }
      res.render('../views/admin/add-schedule.ejs', {
        bus: bus,
        editing: null,
        fullData: allData,
        user: 'Add Schedule',
        userCheck: 'admin',
        errorMessage: null,
      });
    })
    .catch((err) => {
      console.log(err);
    });
};

// Posting schedule details into database
exports.postAddSchedule = async (req, res, next) => {
  const {
    startingPoint,
    destination,
    date,
    departureTime,
    estimatedArrivalTime,
    price,
    busNum,
  } = req.body;

  // Looking for the bus with the bus number provided
  const bus = await Bus.findAll({ where: { busNumber: busNum } });

  // Now using the sequelize method as creatSchedule to add schedule and bus details
  if (bus.length > 0) {
    Bus.findByPk(bus[0].id)
      .then(async (bus) => {
        const schedule = await Schedule.findAll({
          // Checking whether the bus is scheduled in the same day at around same time
          where: { startingPoint: startingPoint, date: date, busId: bus.id },
        });
        // If schedule is found than cannot save in the database
        if (schedule.length > 0) {
          console.log('Cannot enter at the same time'.red);
          const allData = await Bus.findAll();
          if (allData) {
            res.render('../views/admin/add-schedule.ejs', {
              bus: null,
              editing: null,
              fullData: allData,
              user: 'Add Schedule',
              userCheck: 'admin',
              errorMessage: 'This bus has already been scheduled for this day',
            });
          }
        } else {
          bus
            .createSchedule({
              startingPoint: startingPoint,
              destination: destination,
              date: date,
              departureTime: departureTime,
              estimatedArrivalTime: estimatedArrivalTime,
              price: price,
            })
            .then(async (result) => {
              const reservation = await Reservation.findAll();
              const bus = await Bus.findAll();
              res.render('../views/admin/dashboard.ejs', {
                userCheck: 'admin',
                user: 'Dashboard',
                reservation: Object.keys(reservation).length,
                totalBus: Object.keys(bus).length,
              });
            })
            .catch((err) => {
              console.log(err.message);
            });
        }
      })
      .catch((err) => {
        console.log(err);
      });
  } else {
    console.log('There is no schedule added'.red);
    const allData = await Bus.findAll();
    res.render('../views/admin/add-schedule.ejs', {
      bus: null,
      editing: null,
      fullData: allData,
      user: 'Add Schedule',
      userCheck: 'admin',
      errorMessage: 'Some of the fields are empty',
    });
  }
};

// Updating the schedule
exports.getEditSchedule = async (req, res, next) => {
  const scheduleId = req.params.id;
  const data = await Schedule.findAll({ where: { id: scheduleId } });
  const allData = await Bus.findAll();
  const busNum = await data[0].getBus();
  res.render('../views/admin/add-schedule.ejs', {
    editing: 'editmode',
    bus: data,
    fullData: allData,
    busNum: busNum.busNumber,
    user: 'Edit Schedule',
    userCheck: 'admin',
    errorMessage: null,
  });
};

// Save edited schedule
exports.postEditSchedule = async (req, res, next) => {
  const id = req.body.schId;
  const updatedStartingPoint = req.body.startingPoint;
  const updatedDestination = req.body.destination;
  const updatedScheduleDay = req.body.date;
  const updatedDepartureTime = req.body.departureTime;
  const updatedEstimatedArrivalTime = req.body.estimatedArrivalTime;
  const updatedPrice = req.body.price;
  const updatedBusNumber = req.body.id;
  // To insert updated bus id in schedule table foreign key column
  const bus = await Bus.findAll({ where: { id: updatedBusNumber } });
  const updatedBusId = bus[0].id;
  // Updating the schedule column
  await Schedule.update(
    {
      startingPoint: updatedStartingPoint,
      destination: updatedDestination,
      date: updatedScheduleDay,
      departureTime: updatedDepartureTime,
      estimatedArrivalTime: updatedEstimatedArrivalTime,
      price: updatedPrice,
      busId: updatedBusId,
    },
    { where: { id: id } }
  );
  console.log('Hehe'.red);
  res.render('../views/admin/dashboard.ejs', {
    userCheck: 'admin',
    user: 'Dashboard',
  });
};

// @Desc  Delete schedule
exports.deleteSchedule = async (req, res, next) => {
  const scheduleId = req.params.scheduleId;
  const schedule = await Schedule.findByPk(scheduleId);
  schedule.destroy();
  res.render('../views/admin/dashboard.ejs', {
    userCheck: 'admin',
    user: 'Dashboard',
  });
};
