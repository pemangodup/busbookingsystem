const { validationResult } = require('express-validator');
// Import the models ../models/bus
const Bus = require('../../models/bus');
const Reservation = require('../../models/reservation');
const Schedule = require('../../models/schedule');

exports.getSearch = async (req, res, next) => {
  const message = req.flash('error');
  const schedule = await Schedule.findAll();
  res.render('../views/user/search.ejs', {
    schedule: schedule,
    title: 'Search Page',
    error: message,
  });
};

// This controller will be called once after search btn is pressed and if the data is found
exports.postSearch = async (req, res, next) => {
  const { from, to, date } = req.body;
  const errorsResult = validationResult(req);
  const sch = await Schedule.findAll();
  let check = 0;
  const schedule = await Schedule.findAll();

  // Validating whether the given date is bigger than the present date
  var toDate = new Date();
  if (new Date(date).getTime() <= toDate.getTime()) {
    res.render('../views/user/search.ejs', {
      title: 'Book Ticket',
      error: 'The Date must be Bigger than today date',
      schedule: schedule,
    });
  } else {
    // Checking any error habe been passed from the validators
    if (errorsResult.errors.length == 0) {
      // Looping in order to know whether it exists or not
      sch.forEach(async (element) => {
        if (
          element.startingPoint == from &&
          element.destination == to &&
          element.date == date
        ) {
          check++;
        }
      });
      // Now if it exists than go and reade the data in the view
      if (check > 0) {
        const sch = await Schedule.findAll({
          where: { startingPoint: from, destination: to, date: date },
          include: Bus,
        });
        res.render('../views/user/homepage.ejs', {
          details: req.body,
          schedule: sch,
          title: 'Homepage',
          errorMessage: null,
        });
      } else {
        req.flash('error', 'There is no bus in this route');
        res.redirect('/');
      }
      // If there is an empty field than goes inside else condition
    } else {
      // const schedule = await Schedule.findAll();
      res.render('../views/user/search.ejs', {
        editing: null,
        schedule: schedule,
        title: 'Search Page',
        error: errorsResult.errors[0].msg,
      });
    }
  }
};
