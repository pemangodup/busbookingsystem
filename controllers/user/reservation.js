const { validationResult } = require('express-validator');
// Import the models ../models/bus
const Bus = require('../../models/bus');
const Reservation = require('../../models/reservation');
const Schedule = require('../../models/schedule');

// @ Desc GET reservation
exports.getReservation = async (req, res, next) => {
  const reservation = await Reservation.findAll({
    where: {
      bookedBy: req.session.user.email,
    },
  });
  res.render('../views/user/profile.ejs', {
    reservation: reservation,
    user: 'passenger',
    search: 'search',
    userCheck: null,
    error: null,
  });
};

// @Desc    Search reservation by email, name and date
exports.searchReservation = async (req, res, next) => {
  const { searchOption, search, userEmailId } = req.body;
  console.log(`${search}`.red);
  try {
    // Search by date
    if (searchOption == 'byDate') {
      const reservation = await Reservation.findAll({
        where: {
          bookingDate: search,
          bookedBy: req.session.user.email,
        },
      });
      res.render('../views/user/profile.ejs', {
        reservation: reservation,
        user: 'passenger',
        search: 'search',
        userCheck: null,
        error: null,
      });
    }
  } catch (error) {
    console.log(`Error: ${error.message}`.rainbow);
    const reservation = await Reservation.findAll({
      where: {
        bookedBy: req.session.user.email,
      },
    });
    res.render('../views/user/profile.ejs', {
      reservation: reservation,
      user: 'passenger',
      search: 'search',
      userCheck: null,
      error: error.message,
    });
  }
};
