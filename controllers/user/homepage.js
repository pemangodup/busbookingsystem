const { validationResult } = require('express-validator');
// Import the models ../models/bus
const Bus = require('../../models/bus');
const Reservation = require('../../models/reservation');
const Schedule = require('../../models/schedule');

// // Desc GET booking
// exports.getBooking = async (req, res, next) => {
//   const scheduleId = req.body.scheduleId;
//   const schedule = await Schedule.findAll({
//     where: { id: scheduleId },
//     include: Bus,
//   });
//   // const schedule = await Schedule.findAll();
//   res.render('../views/user/booking.ejs', {
//     editing: null,
//     schedule: schedule,
//     title: 'Search Page',
//     error: null,
//   });
// };

// @Desc POST booking
exports.postBooking = async (req, res, next) => {
  const scheduleId = req.body.scheduleId;
  const schedule = await Schedule.findAll({
    where: { id: scheduleId },
    include: Bus,
  });
  // Finding all the reservation with respective schedule id
  let reservation;
  try {
    reservation = await Reservation.findAll({
      where: { scheduleId: scheduleId },
      include: { all: true, nested: true },
    });
  } catch (error) {
    console.log(`${error.message}`.rainbow);
  }

  // Declaring variable
  let reserve = schedule[0].bus.capacity;
  // If reservation exist than checking for the available seats
  if (reservation.length > 0) {
    let seats = new Array();
    reservation.forEach((element) => {
      let seat = element.seats;
      seat = seat.split(',');
      seat.forEach((value) => {
        seats.push(value);
      });
    });
    console.log(seats);
    // // Turning reserve string into array
    // reserve = reserve.split(',');

    // //  Having all the seats in one array
    // let seats = new Array();
    // console.log(reservation);
    // // Finding all the reserved seats
    // reservation.forEach((element) => {
    //   seats = seats + ' ' + element.seats;
    // });
    // seats = seats.trim();
    // seats = seats.split(' ');

    // seatsAvailable = reserve.filter((n) => !seats.includes(n));

    res.render('../views/user/booking.ejs', {
      schedule: schedule,
      seats: seats,
      errorMessage: null,
      title: 'Booking',
    });
  } else {
    // Go to booking page as there is no reservation till date
    res.render('../views/user/booking.ejs', {
      schedule: schedule,
      seats: ['No seat reserved'],
      errorMessage: null,
      title: 'Booking',
    });
  }
};
