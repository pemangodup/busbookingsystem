const { validationResult } = require('express-validator');
const { boolean } = require('webidl-conversions');
// Import the models ../models/bus
const Bus = require('../../models/bus');
const Reservation = require('../../models/reservation');
const Schedule = require('../../models/schedule');

// @Desc Booking tickets or reserving the tickets
exports.postBookTicket = async (req, res, next) => {
  const {
    scheduleId,
    price,
    bookingDate,
    // A1,
    // B1,
    // C1,
    // D1,
    // A2,
    // B2,
    // C2,
    // D2,
    // A3,
    // B3,
    // C3,
    // D3,
    // A4,
    // B4,
    // C4,
    // D4,
    // A5,
    // B5,
    // C5,
    // D5,
  } = req.body;
  const bodyData = req.body;
  const userName = req.session.user.name;
  const email = req.session.user.email;
  const errorsResult = validationResult(req);

  // Initiating array varaible and finding the total amount
  var seatsArray = new Array();
  for (const element in bodyData) {
    if (
      element != 'scheduleId' &&
      element != 'price' &&
      element != 'bookingDate' &&
      element != '_csrf' &&
      element != 'seatsAvailable'
    ) {
      seatsArray.push(element);
    }
  }
  let totalAmount;
  if (seatsArray.length > 0) {
    const length = await seatsArray.length;
    totalAmount = price * length;
  }
  // Finding the specific Schedule for the purpose of printing the details
  const schedule = await Schedule.findAll({
    where: { id: scheduleId },
    include: Bus,
  });
  // For the purpose of checking whethter the not available seat have been selectd
  const reservation = await Reservation.findAll({
    where: { scheduleId: scheduleId, bookingDate: bookingDate },
    include: { all: true, nested: true },
  });
  //  Declaring an array
  let seats = new Array();

  try {
    // Finding all the reserved seats
    reservation.forEach((element) => {
      let seat = element.seats;
      seat = seat.split(',');
      seat.forEach((value) => {
        seats.push(value);
      });
    });
  } catch (error) {
    console.log(error);
  }

  if (totalAmount > 0) {
    // If value in seat is selected than it goes inside into if statement
    if (errorsResult.errors.length <= 0) {
      if (reservation.length > 0) {
        let num = 0;
        const sendSeats = seats;
        seatsArray.forEach((element) => {
          let res = seats.includes(element);
          if (res == true) {
            num = num + 1;
          }
          let index = seats.indexOf(element);
          seats.splice(index, 1);
        });
        if (num > 0) {
          const schedule = await Schedule.findAll({
            where: { id: scheduleId },
            include: Bus,
          });
          res.render('../views/user/booking.ejs', {
            title: 'Book Ticket',
            seats: sendSeats,
            errorMessage: 'Seat is already reserved',
            schedule: schedule,
          });
        } else {
          schedule[0].createReservation({
            numberOfSeats: seatsArray.length,
            seats: seatsArray.toString(),
            totalAmount: totalAmount,
            bookingDate: bookingDate,
            name: userName,
            bookedBy: email,
            ticketNumber: bookingDate + scheduleId + seats,
          });
          console.log('Added');
          if (req.session.user.userType === 'admin') {
            res.redirect('/admin/dashboard');
          } else {
            res.redirect('/profile');
          }
        }
      } else {
        // When there is no reservation at all for this date than reserve in this date
        schedule[0].createReservation({
          numberOfSeats: seatsArray.length,
          seats: seatsArray.toString(),
          totalAmount: totalAmount,
          bookingDate: bookingDate,
          name: userName,
          bookedBy: email,
          ticketNumber: bookingDate + scheduleId,
        });
        console.log('Added');
        if (req.session.user.userType === 'admin') {
          res.redirect('/admin/dashboard');
        } else {
          res.redirect('/profile');
        }
      }
    } else {
      res.render('../views/user/booking.ejs', {
        title: 'Book Ticket',
        seats: sendSeats,
        errorMessage: errorsResult.errors[0].msg,
        schedule: schedule,
      });
    }
  } else {
    res.render('../views/user/booking.ejs', {
      title: 'Book Ticket',
      seats: seats,
      errorMessage: 'No seat selected',
      schedule: schedule,
    });
  }
};
