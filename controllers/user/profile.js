const { validationResult } = require('express-validator');
// Import the models ../models/bus
const Bus = require('../../models/bus');
const Reservation = require('../../models/reservation');
const Schedule = require('../../models/schedule');

// @Desc GET the profile
exports.getProfile = async (req, res, next) => {
  const userEmailId = req.session.user.email;
  const reservatin = await Reservation.findAll({
    where: { bookedBy: userEmailId },
  });
  res.render('../views/user/profile.ejs', {
    user: 'Reservation List',
    userEmailId: userEmailId,
    reservation: reservatin,
    userCheck: null,
    error: null,
  });
};
