const express = require('express');
const color = require('colors');
const dotenv = require('dotenv');
const sequelize = require('./util/database');
const authRoutes = require('./routes/auth');
const adminRoutes = require('./routes/admin');
const searchRoutes = require('./routes/search');
const Schedule = require('./models/schedule');
const Reservation = require('./models/reservation');
const Bus = require('./models/bus');
const session = require('express-session');

const csrf = require('csurf');
// initalize sequelize with session store
var SequelizeStore = require('connect-session-sequelize')(session.Store);

// Initializing csrf session protection
const csrfProtection = csrf();

const path = require('path');
const flash = require('connect-flash');

const app = express();

//  Initializing dotenv
dotenv.config({ path: 'config/config.env' });

// Defining engines or setting the view engine
app.set('view engine', 'ejs');

// Finding the static page for css, image and js
app.use(express.static(path.join(__dirname + '/public')));

// Adding a body parser
app.use(express.urlencoded({ extended: false }));

// Enabling connect flash so that all request will have req.flash() function
app.use(flash());

// Initializing session
app.use(
  session({
    secret: 'secret key',
    resave: false,
    saveUninitialized: false,
    store: new SequelizeStore({
      db: sequelize,
    }),
  })
);

// Creating a middleware
app.use(csrfProtection);

// Sending data to our views
app.use((req, res, next) => {
  res.locals.isLoggedIn = req.session.isLoggedIn;
  res.locals.csrfToken = req.csrfToken();
  next();
});

// Routes
// Middleware for auth routes
app.use('/auth', authRoutes);
app.use('/admin', adminRoutes);
app.use('/', searchRoutes);

// create,
Bus.hasMany(Schedule);
Schedule.belongsTo(Bus);
Schedule.hasMany(Reservation);
Reservation.belongsTo(Schedule);
sequelize
  // .sync({ force: true })
  .sync()
  .then(() => {
    app.listen(process.env.PORT || 5000, function () {
      console.log(`Server is running on port no ${process.env.PORT}.`.rainbow);
    });
  })
  .catch((err) => {
    console.log(err);
  });
