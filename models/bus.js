const Sequelize = require('sequelize');

const sequelize = require('../util/database');

const Bus = sequelize.define('bus', {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true,
  },
  busName: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  busNumber: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },
  busPlateNumber: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  capacity: {
    type: Sequelize.STRING,
    allowNull: false,
  },
});

module.exports = Bus;
