const Sequelize = require('sequelize');
const sequelize = require('../util/database');

const Reservation = sequelize.define('reservation', {
  id: {
    type: Sequelize.INTEGER,
    allowNull: false,
    primaryKey: true,
    autoIncrement: true,
  },
  numberOfSeats: {
    type: Sequelize.INTEGER,
    allowNull: false,
  },
  seats: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  totalAmount: {
    type: Sequelize.DOUBLE,
    allowNull: false,
  },
  bookingDate: {
    type: Sequelize.DATEONLY,
    allowNull: false,
  },
  name: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  bookedBy: {
    type: Sequelize.STRING,
    allowNull: false,
  },
  ticketNumber: {
    type: Sequelize.STRING,
    allowNull: false,
  },
});

module.exports = Reservation;
