// Importing sequelize package
const Sequelize = require('sequelize');

// Importing database connection
const sequelize = require('../util/database');

// Creating signup model
const User = sequelize.define('user', {
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    allowNull: false,
    primaryKey: true,
  },
  name: Sequelize.STRING,
  email: Sequelize.STRING,
  password: Sequelize.STRING,
  userType: Sequelize.STRING,
  token: Sequelize.STRING,
  tokenExpiration: Sequelize.DATE,
});

module.exports = User;
