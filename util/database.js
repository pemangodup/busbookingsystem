// Importing the package
const dotenv = require('dotenv');
const Sequelize = require('sequelize');

// Configuring the dotenv file
dotenv.config({ path: './config/config.env' });

// Connecting to the database
const sequelize = new Sequelize(
  process.env.DATABASE,
  process.env.USERNAME,
  process.env.PASSWORD,
  {
    dialect: 'mysql',
    host: 'localhost',
    Storage: './session.sqlite',
  }
);
module.exports = sequelize;
