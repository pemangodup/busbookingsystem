# Bus Reserving App

## About

It is a bus seat reservation app. With the help of it customer can book a ticket online once they are logged in. And admin has the permission to add, edit and delete the bus, schedule, etc.

## Installation

### Before running the app. Some of the requirements for configuration are:

1. PORT =
2. LOCALHOST = ''
3. USERNAME = ''
4. PASSWORD = ''
5. DATABASE = ''

6. API_KEY = It is a sendgrid for mailing purpose. Need to create an account in sendgrid and get the api. **''SG.7iU6a49dSdqSkrghzidn0g.cBFC_7HjQNTHWex3PUQeidvk7erLaW4v8GxF-HkLhU'**

### Now App is ready to run

1. Can run from dev dependencies using _npm run dev_

## First time running

### At first

Process to follow as there will be no data in the beginning

- Create an account by clicking profile and as we are not logged in it will take us to login and sign up page.
- Sign up
- Password length must be greater than 5 and max to 20
- Email id must be real. So, can retrieve the password in future

Need to open database and in userType manually add `admin` in the place of `passenger` for to make it an admin. This was done in order to maintain ony admin user.

### Now once the admin is created

- Go to login by clicking profile and login using details such as email id and password.

### After login

- Add Bus in the beginning:

  1. Click `add bus` under features in menu bar
  2. Give all the details.
  3. No same bus number (`3503`) with same number plate(`Ba 13`) can be added.

- Add schedule which is under the features category in menu bar
  1. Click `add schedule` and give all the details.
  2. No same bus can be added in same date. Bus can add in the next day, assuming that bus returned from the destination to source in the same date. And did two way route.
- List of reservation
  1. Admin can see all the list of reservation
  2. Can search based on `date`, `name`, `emailId`.

## Passenger view

- Search for bus

  1. Insert from and to and a particular date.
  2. Cannot enter past date. And should only be future one.
  3. If no bus on the date than it gives us message that there is no bus in this route.

- Select the bus

  1. Now from the list of bus on the particular date. Selet the one.
  2. It will get to next page where selection of available seats can be done.

- Select the seats
  1. If tried to select the already reserved seats than we get error message as `Seat is already reserved`.
  2. If tried to book without selecting any seats we get error message `No seat selected`.
  3. Once selected than seat will be reserved and we will be transfered to reservation list page of our profile.

> Some of the main tools used while creating this app

- Visual studio
- Framework
  - Express
- Language
  - JavaScript
- Database
  - MySql
- Databse ORM
  - Sequelize
